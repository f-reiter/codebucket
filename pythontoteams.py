import pymsteams
import psutil
import os

# This webhook URL refers to the TEAMS channel PIMCORE-->System Updates
myTeamsMessage = pymsteams.connectorcard("https://ledvance365.webhook.office.com/webhookb2/62a01d40-230a-4a80-b064-080f8e52757c@e13959ad-6279-420c-aba3-a9e83c85ad2e/IncomingWebhook/b9fa548b92aa4b7aa1782b4eaec207dd/32b22e98-3e53-47fe-a37c-1f0a68faa9af")

load1, load5, load15 = psutil.getloadavg()

cpu_usage = (load1/os.cpu_count()) * 100
  

if cpu_usage > 30:
    myTeamsMessage.text("Dear users, the PIMCORE server is on high load and may react slower then expected.")
    myTeamsMessage.send()
else:
    pass