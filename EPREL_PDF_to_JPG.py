# Script based on the chatGPT 3.5. prompt:
# "You are a python backend programmer and code in Linux. Write me a script that reads an excel file which has two columnes ID and LANGUAGE. the script shall then take each line and download a pdf file from a URL that takes the ID and the language as parameter in the route. Those files need to be stored in a folder with the that has the same name as the excel file plus the date (to be unique) and store the files there  .The file then needs to be converted from PDF to JPG in one file (inkscape and the "convert" command are available) and stored in the same folder."
# Slightly modified to use convert from Imagick instead of inkscape.
# Further improved: have more languages in the excel
# Further improved: Renaming of result file

import os
import requests
import subprocess
from datetime import datetime
from pathlib import Path
from openpyxl import load_workbook

def log_message(message):
    """
    Logs a message to a log file in the 'log' directory (one level up from the script directory).
    """
    log_dir = Path(__file__).parent.parent / "log"
    log_dir.mkdir(parents=True, exist_ok=True)
    log_file = log_dir / "script.log"
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open(log_file, "a") as f:
        f.write(f"[{timestamp}] {message}\n")

def download_pdf(id, language, output_folder, gtin):
    """
    Downloads a PDF file from a given URL using the provided ID and language.
    The file is saved in the specified output folder.
    
    Args:
        id (str): The document ID.
        language (str): The language code.
        output_folder (Path): Directory to save the downloaded PDF.
        gtin (str): The GTIN number.
    
    Returns:
        Path or None: The path to the downloaded PDF file or None if an error occurs.
    """
    url = f"https://eprel.ec.europa.eu/api/product/{id}/fiches?noRedirect=false&language={language}"
    pdf_path = output_folder / f"{gtin}_{id}_{language}.pdf"
    
    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()
        with open(pdf_path, 'wb') as f:
            for chunk in response.iter_content(1024):
                f.write(chunk)
        log_message(f"Successfully downloaded PDF: {pdf_path}")
        return pdf_path
    except requests.RequestException as e:
        log_message(f"Error downloading PDF ({id}, {language}): {e}")
        return None

def convert_pdf_to_jpg(pdf_path):
    """
    Converts a given PDF file to a JPG format using GraphicsMagick (gm convert tool).
    
    Args:
        pdf_path (Path): The path of the PDF file to be converted.
    
    Returns:
        Path or None: The path to the converted JPG file or None if an error occurs.
    """
    jpg_path = pdf_path.with_suffix('.jpg')
    try:
        subprocess.run(["gm", "convert", "-append", str(pdf_path), str(jpg_path)], check=True)
        log_message(f"Successfully converted PDF to JPG: {jpg_path}")
        return jpg_path if jpg_path.exists() else None
    except subprocess.CalledProcessError as e:
        log_message(f"Error converting PDF to JPG ({pdf_path}): {e}")
        return None

def main():
    """
    Main function to process an Excel file containing GTIN, document IDs, and language codes,
    download corresponding PDFs, convert them to JPG, and clean up the input file.
    """
    script_dir = Path(__file__).parent
    excel_files = list(script_dir.glob("*.xlsx"))
    
    if not excel_files:
        log_message("No Excel files found in the script directory.")
        print("No Excel files found in the script directory.")
        return
    
    excel_file = excel_files[0]
    wb = load_workbook(excel_file)
    sheet = wb.active
    
    # Create an output folder with a unique timestamp
    output_folder = script_dir / f"{excel_file.stem}_{datetime.now().strftime('%Y%m%d_%H%M%S')}"
    output_folder.mkdir(parents=True, exist_ok=True)
    
    jpg_paths = []
    
    # Process each row in the Excel file (skipping header)
    for row in sheet.iter_rows(min_row=2, values_only=True):
        id, language, gtin = row
        pdf_path = download_pdf(id, language, output_folder, gtin)
        if pdf_path:
            jpg_path = convert_pdf_to_jpg(pdf_path)
            if jpg_path:
                jpg_paths.append(jpg_path)
    
    log_message("PDFs downloaded and converted to JPG successfully.")
    print("PDFs downloaded and converted to JPG successfully.")
    print("JPG files are saved in:", output_folder)
    
    # Remove input Excel file after processing
    excel_file.unlink()
    log_message(f"Input Excel file '{excel_file}' removed.")
    print(f"Input Excel file '{excel_file}' removed.")
    
    return jpg_paths

if __name__ == "__main__":
    main()