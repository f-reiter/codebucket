#!/bin/bash

#Batch converts PDF to png

ext="png"
for i in *.pdf; do
    name=$(basename "$i" pdf)
    convert -density 72 -append $i "$name$ext"
done
