# This script reads all files in the folder and
# outputs the IDs of the XML

# There are some sample files in the repository added.


import glob
from xml.etree import ElementTree

# glob to get multiple filenames in the folder
filenames = glob.glob("master*.xml")  # change the pattern to match your case
numberofitems = 0
numberofic = 0
itemset = set(())
result = open("result.txt", 'w+')
usicid = "OS"
numberofusproducts = 0
# We itterate over each item in the folder
for filename in filenames:

# To be sure we print the filename and also use it as a seperator
    result.write(filename)
    result.write('\n')
    
# We open one file in UTF8
    with open(filename, 'rt', encoding='UTF8') as f:
        tree = ElementTree.parse(f)
# After the XML tree is parsed we can check every tag with product and the
# ID and UserTypeID
    for node in tree.iter('Product'):
        name = node.attrib.get('ID')
        type = node.attrib.get('UserTypeID')
        #This is basically not needed as we always expect value
        if type:
            result.write('  %s :: %s ' % (name, type))
            result.write('\n')
            itemset.add(name)
            numberofitems = numberofitems + 1
        else:
            result.write(name)
        if type == "IC":
            numberofic = numberofic + 1
            if usicid in name:
                numberofusproducts = numberofusproducts +1
        
    # close the file - should save memory, right?
    f.close()
# And we print the total number of objects
result.write('Total number of objects: ' + str(numberofitems))
result.write('\n')
result.write('Total UNIQUE number of objects: ' + str(len(itemset)))
result.write('\n')
result.write('Total number of IC objects: ' + str(numberofic))
result.write('\n')
result.write('Total number of US IC objects: ' + str(numberofusproducts))
result.close()
