#!/bin/sh

#Batch converts all eps files in a folder to SVG files

for file in *.eps; do
            inkscape -f "$file" --export-plain-svg="${file%.*}.svg"
done